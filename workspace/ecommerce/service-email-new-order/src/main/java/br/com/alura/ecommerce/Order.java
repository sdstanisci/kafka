package br.com.alura.ecommerce;

import java.math.BigDecimal;

public class Order {

	private final String email;
	private final String orderId;
	private final BigDecimal amount;
	
	public Order(String email, String orderId, BigDecimal amount) {
		super();
		this.orderId = orderId;
		this.amount = amount;
		this.email = email;
	}
	
	public BigDecimal getAmount() {
		return amount;
	}

	@Override
	public String toString() {
		return "Order [email=" + email + ", orderId=" + orderId + ", amount=" + amount + "]";
	}

	public String getEmail() {
		return email;
	}

}
