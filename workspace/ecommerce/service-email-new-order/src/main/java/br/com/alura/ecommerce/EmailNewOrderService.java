package br.com.alura.ecommerce;

import java.math.BigDecimal;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import org.apache.kafka.clients.consumer.ConsumerRecord;

import br.com.alura.ecommerce.consumer.KafkaService;
import br.com.alura.ecommerce.dispatcher.KafkaDispatcher;

public class EmailNewOrderService {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        var emailService = new EmailNewOrderService();
        try (var service = new KafkaService<>(EmailNewOrderService.class.getSimpleName(),
                "ECOMMERCE_NEW_ORDER",
                emailService::parse,
                Map.of())) {
            service.run();
        }
    }
    
    private final KafkaDispatcher<String> emailDispatcher = new KafkaDispatcher<>();

    private void parse(ConsumerRecord<String, Message<Order>> record) {
        System.out.println("------------------------------------------");
        System.out.println("Processing new order, preparing email");
        System.out.println(record.key());
        
        Message<Order> message = record.value();
		
        System.out.println(message);
        System.out.println(record.partition());
        System.out.println(record.offset());
        System.out.println("Order processed");
    
        var emailCode = "Obrigado por enviar a order, estamos processando";
        
        Order order = message.getPayload();
        
		CorrelationId id = message.getId().continueWith(EmailNewOrderService.class.getSimpleName());
		
		try {
			
			emailDispatcher.send("ECOMMERCE_SEND_EMAIL",
					order.getEmail(),
					id,
					emailCode);
		
		} catch (ExecutionException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
    }

	private boolean isFraud(Order order) {
		return order.getAmount().compareTo(new BigDecimal(4500)) > 0;
	}

}