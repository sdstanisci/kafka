package br.com.alura.ecommerce;

public class User {

	private final String uuid;

	public User(String uuid) {
		super();
		this.uuid = uuid;
	}

	public String getUuid() {
		return uuid;
	}
}
