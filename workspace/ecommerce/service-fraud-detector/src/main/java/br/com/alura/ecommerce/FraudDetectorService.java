package br.com.alura.ecommerce;

import java.math.BigDecimal;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.apache.kafka.clients.consumer.ConsumerRecord;

import br.com.alura.ecommerce.consumer.KafkaService;
import br.com.alura.ecommerce.dispatcher.KafkaDispatcher;

public class FraudDetectorService {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        var fraudService = new FraudDetectorService();
        try (var service = new KafkaService<>(FraudDetectorService.class.getSimpleName(),
                "ECOMMERCE_NEW_ORDER",
                fraudService::parse,
                //Order.class,
                Map.of())) {
            service.run();
        }
    }
    
    private final KafkaDispatcher<Order> orederDispatcher = new KafkaDispatcher<>();

    private void parse(ConsumerRecord<String, Message<Order>> record) {
        System.out.println("------------------------------------------");
        System.out.println("Processing new order, checking for fraud");
        System.out.println(record.key());
        System.out.println(record.value());
        System.out.println(record.partition());
        System.out.println(record.offset());
        System.out.println("Order processed");
    
        var message = record.value();
        
        var order = message.getPayload();
        
        try {
	        if(isFraud(order)) {

	        	System.out.println("Eh fraude ! ".toString());
				orederDispatcher.send("ECOMMERCE_ORDER_REJECTED", 
						order.getEmail(), 
						message.getId().continueWith(FraudDetectorService.class.getSimpleName()),
						order);
	        
	        }else {
	        
	        	System.out.println("Não eh fraude ! ".toString());
	        	orederDispatcher.send("ECOMMERCE_ORDER_APPROVED", 
	        			order.getEmail(), 
	        			message.getId().continueWith(FraudDetectorService.class.getSimpleName()),
	        			order);

	        }
        } catch (Exception e) {
			e.printStackTrace();
        }
    }

	private boolean isFraud(Order order) {
		return order.getAmount().compareTo(new BigDecimal(4500)) > 0;
	}

}