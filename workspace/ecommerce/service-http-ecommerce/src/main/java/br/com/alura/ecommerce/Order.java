package br.com.alura.ecommerce;

import java.math.BigDecimal;

public class Order {

	private final String orderId;
	private final BigDecimal amount;
	
	public Order(String orderId, BigDecimal amount, String email) {
		super();
		this.orderId = orderId;
		this.amount = amount;
	}
	
	public BigDecimal getAmount() {
		return amount;
	}

	@Override
	public String toString() {
		return "Order [orderId=" + orderId + ", amount=" + amount + "]";
	}
	
	
}
