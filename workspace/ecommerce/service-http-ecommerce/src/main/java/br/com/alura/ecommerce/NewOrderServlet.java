package br.com.alura.ecommerce;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.alura.ecommerce.dispatcher.KafkaDispatcher;

public class NewOrderServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2020733420529450762L;
	
	private final KafkaDispatcher<Order> orderDispatcher = new KafkaDispatcher<>();
	//private final KafkaDispatcher<Email> emailDispatcher = new KafkaDispatcher<>();
	
	@Override
	public void destroy(){
		super.destroy();
		
		orderDispatcher.close();
		//emailDispatcher.close();
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		try {
		
			var orderId = UUID.randomUUID().toString();
	        var amount = new BigDecimal(req.getParameter("amount"));
	        var email = req.getParameter("email");
	        
	        var order = new Order(orderId, amount, email);
	        orderDispatcher.send("ECOMMERCE_NEW_ORDER", email, 
	        		new CorrelationId(NewOrderServlet.class.getSimpleName()),
	        		order);
			
//	        var emailCode = new Email(orderId, "Thank you for your order! We are processing your order!");
//			emailDispatcher.send("ECOMMERCE_SEND_EMAIL", email, 
//					new CorrelationId(NewOrderServlet.class.getSimpleName()),
//					emailCode);

	        resp.getWriter().println("New Order send successfuly.");
			resp.setStatus(200);

		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
